:PROPERTIES:
:ID:       c22938ac-2d6f-4b43-bbbb-7ba5c656f89a
:END:
#+title: Workstation

* About
Notes re how people set up their workstations
* The idea is: "I erase my systems at every boot"
[[file:20210417000154-nix.org::#20210417000154-nix-org--erase-your-darlings][20210417000154-nix.org::erase your darlings]]

I love this idea for my own workstation(s).
